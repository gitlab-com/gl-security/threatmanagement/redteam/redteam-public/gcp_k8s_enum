#!/usr/bin/env python3

"""
Consolidate kubectl output of multiple GKE projects to show ingress.

Please see the included README for detailed instructions.
"""

import glob
import sys
import os
import json
import argparse
import time

# You can change these globals if you are using BigQuery (optional)
BQDATASET = "resources"
BQTABLE = "details"

def process_args():
    """Handles user-passed parameters"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i', type=str, action='store', required=True,
                        help='Directory containing output of gather-data.sh shell script')
    parser.add_argument('--bigquery', '-bg', type=str, action='store',
                        help='Load data into specified BigQuery project ID')

    args = parser.parse_args()

    if not os.path.isdir(args.input):
        print("[!] That directory does not exist. Please try again.")
        sys.exit()

    return args

def parse_json(file):
    """
    Loads the json data from a file into memory
    """
    with open(file, 'r') as infile:
        try:
            data = json.load(infile)
        except json.decoder.JSONDecodeError:
            print("[!] Error decoding file: {}".format(file))
            return {}

    return data

def extract_ingresses(file):
    """
    Extracts the ingress definitions from a file, tagging them with the
    GCP project and GKE cluster name gleaned from file path info
    """
    ingresses = []
    cluster = os.path.basename(file).replace(".json", "")
    project = os.path.dirname(file).split("/")[-1]
    resource_type = 'Google Kubernetes Ingress'
    tcp = ""
    udp = ""

    data = parse_json(file)
    for item in data['items']:
        name = item['metadata']['name']
        id = item['metadata']['uid']
        identifier = 'Project: {} / Cluster: {} / Ingress: {}'.format(project, cluster, name)
        if 'rules' in item['spec']:
            for rule in item['spec']['rules']:
                if 'host' in rule:
                    address = rule['host']
                    ingresses.append('{},{},{},{},{},{},{}'.format(id, name, resource_type, address, tcp, udp, identifier))
                elif 'ingress' in item['status']['loadBalancer']:
                    for addr in item['status']['loadBalancer']['ingress']:
                        address= addr['ip']
                        ingresses.append('{},{},{},{},{},{},{}'.format(id, name, resource_type, address, tcp, udp, identifier))
    return ingresses

def write_file(ingresses):
    """
    Writes a CSV file to the current directory
    """
    csv_file = "ingresses-{}.csv".format(time.time())
    ingresses = set(ingresses)

    with open(csv_file, 'w') as outfile:
        outfile.write("id,name,resourceType,externalAddress,allowedTCP,allowedUDP,identifier\n")
        for ingress in ingresses:
            outfile.write(ingress + "\n")

    print("[*] Wrote {} unique lines to {}".format(len(ingresses), csv_file))

    return csv_file


def process_bigquery(project_id, csv_file):
    """
    Leverages application-default credentials to load CSV into BigQuery
    """
    try:
        from google.cloud import bigquery
    except ImportError:
        print("[!] Please pip3 install --upgrade google-cloud-bigquery!")
        return

    print("[*] Connecting to BigQuery...")

    client = bigquery.Client(project=project_id)

    dataset_ref = client.dataset(BQDATASET)
    table_ref = dataset_ref.table(BQTABLE)
    job_config = bigquery.LoadJobConfig()
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.skip_leading_rows = 1
    job_config.allow_jagged_rows = True

    print("[*] Clearing out existing BigQuery rows")
    query = "delete from {}.{} where resourceType = 'Google Kubernetes Ingress'".format(BQDATASET, BQTABLE)
    job = client.query(query)
    job.result()

    with open(csv_file, "rb") as source_file:
        job = client.load_table_from_file(source_file, table_ref, job_config=job_config)
        result = job.result()
        print("[+] Loaded {} new rows".format(result.output_rows))


def main():
    """
    Main function to parse json files and write analyzed output
    """
    args = process_args()

    cluster_files = []
    ingresses = []

    project_dirs = glob.glob(args.input + '/*')

    for directory in project_dirs:
        files = glob.glob(directory + "/*.json")
        for file in files:
            cluster_files.append(file)

    print("[*] Found {} cluster files, now processing".format(len(cluster_files)))

    for file in cluster_files:
        for ingress in extract_ingresses(file):
            ingresses.append(ingress)

    print("[*] Found {} defined ingresses, writing to file".format(len(ingresses)))

    csv_file = write_file(ingresses)

    # If integrating with GCP BigQuery, do that now
    if args.bigquery:
        process_bigquery(args.bigquery, csv_file)


if __name__ == '__main__':
    main()
